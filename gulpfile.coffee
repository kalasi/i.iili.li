gulp = require('gulp')
concat = require('gulp-concat')
gutil = require('gulp-util')
jade = require('gulp-jade')
rename = require('gulp-rename')
sass = require('gulp-ruby-sass')
scsslint = require('gulp-scss-lint')
sourcemaps = require('gulp-sourcemaps')
uglify = require('gulp-uglify')
watch = require('gulp-watch')

sassLintReporter = (file) ->
  if !file.scsslint.success
    issueCount = file.scsslint.issues.length

    gutil.log file.path + ': ' + issueCount + ' issue' + (if issueCount != 1 then 's' else '') + '.'

    x = 0
    y = file.scsslint.issues.length

    while x < y
      error = file.scsslint.issues[x]
      pads = 3

      message = ''
      message += '    [Line '
      message += pad(error.line, pads) + ']'
      message += '[' + error.severity + ']'
      message += ' ' + error.linter + ': ' + error.reason

      gutil.log message

      x++

  return

# Sass Lint settings
sassLintSettings =
  config: 'resources/gulp/scss-lint/config.yml'
  source: [
    'scss/*.scss'
    'scss/modules/*.scss'
    'scss/modules/**/*.scss'
    'scss/partials/*.scss'
    'scss/partials/**/*.scss'
  ]

# Sass setup
sassSettings =
  destination: './'
  options:
    cacheLocation: 'storage/.sass-cache'
    sourcemap: true
    sourcemapPath: @destination
    stopOnError: true
    style: 'compressed'
    unixNewlines: true
  source: 'resources/scss/app.scss'

pad = (str, precision) ->
  str = str.toString()

  if str.length < precision
    pad('0' + str, precision)
  else
    str

# Compiles everything.
gulp.task 'compile', ->
  gulp.start 'fonts'
  gulp.start 'jade'
  gulp.start 'sass'

  return

gulp.task 'default', ->
  gulp.start 'compile'

  return

gulp.task 'fonts', ->
  gulp.src './bower_components/fontawesome/fonts/*'
    .pipe(gulp.dest('./public/fonts'));

gulp.task 'lint-sass', ->
  gulp.src(sassLintSettings.source)
    .pipe scsslint(
      config: sassLintSettings.config
      customReport: sassLintReporter
    )

gulp.task 'jade', ->
  gulp.src('./resources/jade/**/*.jade')
    .pipe(jade({}))
    .pipe(rename((path) ->
      path.extname = '.blade.php'
    ))
    .pipe(gulp.dest('./resources/views'))

# Runs a compile and starts watching.
gulp.task 'start', ->
  gulp.start 'compile'
  gulp.start 'watch'

  return

# Compiles the Sass into a CSS file.
gulp.task 'sass', ->
  sass('./resources/scss/', sassSettings.options)
    .on('error', (err) ->
      console.error 'Error!', err.message

      return
    )
    .pipe(gulp.dest('./public'))

gulp.task 'watch', ->
  gulp.watch './resources/scss/**/*.scss', [ 'sass' ]

  return
