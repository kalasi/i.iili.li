<?php
namespace App\Console\Commands\Temporary;

use App\File;
use Illuminate\Console\Command;

class Clear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'temporary:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears temporary files past the expiration date.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $weekAgoUnix = time() - (86400 * 7);
        $weekAgo = date('Y-m-d H:i:s', $weekAgoUnix);

        $files = File::where('type', File::TYPE_TEMPORARY)
            ->where('created_at', '<=', $weekAgo)
            ->get();

        foreach ($files as $file) {
            $name = $file->name;

            $file->delete();

            unlink(storage_path('upload/' . $name));

            $this->line('Deleted ' . $name);
        }
    }
}
