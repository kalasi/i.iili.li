<?php

use App\File;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

define('UPLOAD', storage_path('upload/'));

function curl($url) {
    $ch = curl_init();

    $timeout = 3;

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0');

    $data = curl_exec($ch);

    curl_close($ch);

    return $data;
}

function getName() {
    $chars = array_flatten([range('a', 'z'), range('A', 'Z'), range(0, 9)]);

    $params = http_build_query([
        'c' => implode('', $chars),
        'l' => 4,
    ]);

    $name = '';

    while (true) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://g.iili.li');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $name = curl_exec($ch);
        curl_close($ch);

        $file = File::where('name', '=', $name);

        if (!$file->exists()) {
            break;
        }
    }

    return $name;
}

$app->get('/', function() use ($app) {
    return view('index');
});

$app->post('u', function () {
    $name = getName();
    $temporary = \Request::get('temporary') === 'on'
        ? File::TYPE_TEMPORARY
        : File::TYPE_PERMANENT;

    File::create([
        'name' => $name,
        'type' => $temporary,
    ]);

    \Request::file('image')->move(UPLOAD, $name);

    return redirect()->to($name);
});

$app->post('url', function () {
    $url = \Request::get('url');
    $temporary = \Request::get('temporary') === 'on'
        ? File::TYPE_TEMPORARY
        : File::TYPE_PERMANENT;

    $name = getName();

    $file = curl($url);

    file_put_contents(UPLOAD . $name, $file);

    File::create([
        'name' => $name,
        'type' => $temporary,
    ]);

    return redirect()->to($name);
});

// Image.
$app->get('{image:.*}', function ($image) {
    $image = urldecode($image);
    $path = UPLOAD . $image;

    $file = File::where('name', '=', $image)->first();

    if (empty($file) || !file_exists($path)) {
        $data = compact('image');
        return view('missing', $data);
    }

    $mime = mime_content_type($path);

    header('Content-Type: ' . $mime);

    echo file_get_contents($path);

    return;
});
